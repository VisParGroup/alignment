![1410200024-alignment-1.jpg](https://bitbucket.org/repo/zy8bzp/images/2563743095-1410200024-alignment-1.jpg)

alignment package for R 
====
Developed by the VisPar Group of the Laboratory of Nonlinear Dynamics, Institute of Cybernetics at Tallinn University of Technology, alignment is an R package for calculating orientation parameters, orientation tensors, alignment tensors and the orientation distribution function for short fibres. This package is mainly used within our research on Steel Fibre Reinforced Cementitious Composites.

Usage
----
```
#!r
  sourceDir <- function(path, trace = TRUE, ...) {
    for (nm in list.files(path, pattern = "\\.[RrSsQq]$")) {
       if(trace) cat(nm,":")           
       source(file.path(path, nm), ...)
       if(trace) cat("\n")
    }
  }
```


To import all functions in the directory:
```
#!r
  sourceDir("/path/to/alignment/R")
```

Or, with devtools installed:
```
#!r
install_bitbucket(alignment, vispar, ref = "master", branch = NULL,
  auth_user = NULL, password = NULL, ...)
```



Citation
----
This package is based on code and functions originally developed for the following publications (the functions have been re-organized), please cite these if you use the package:

1. Jussi-Petteri Suuronen, Aki Kallonen, Marika Eik, Jari Puttonen, Ritva Serimaa, and Heiko Herrmann. Analysis of short fibres orientation in steel fibre reinforced concrete (SFRC) using x-ray tomography. Journal of Materials Science, 48(3):1358-1367, February 2013. DOI:[10.1007/s10853-012-6882-4](http://dx.doi.org/10.1007/s10853-012-6882-4)

2. Marika Eik, Karl Lõhmus, Martin Tigasson, Madis Listak, Jari Puttonen, and Heiko Herrmann. DC-conductivity testing combined with photometry for measuring fibre orientations in SFRC. Journal of Materials Science, 48(10):3745-3759, May 2013. DOI:[10.1007/s10853-013-7174-3](http://dx.doi.org/10.1007/s10853-013-7174-3)

3. Heiko Herrmann, Marika Eik, Viktoria Berg, and Jari Puttonen. Phenomenological and numerical modelling of short fibre reinforced cementitious composites. Meccanica, 49(8):1985-2000, August 2014. DOI:[10.1007/s11012-014-0001-3](http://dx.doi.org/10.1007/s11012-014-0001-3)

4. Heiko Herrmann, Emiliano Pastorelli, Aki Kallonen, and Jussi-Petteri Suuronen. Methods for fibre orientation analysis of x-ray tomography images of steel fibre reinforced concrete (sfrc). Journal of Materials Science,  2016. DOI: [10.1007/s10853-015-9695-4](http://dx.doi.org/10.1007/s10853-015-9695-4)

5. Marika Eik, Jari Puttonen, and Heiko Herrmann. The effect of approximation accuracy of the orientation distribution function on the elastic properties of short fibre reinforced composites. Composite Structures, 148:12-18, 2016. DOI: [10.1016/j.compstruct.2016.03.046](http://dx.doi.org/10.1016/j.compstruct.2016.03.046)

____
Needs: GNU R 

Needs R packages: akima, ggplot2, abind

Suggests R packages: plotrix
____
## Disclaimer ##

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
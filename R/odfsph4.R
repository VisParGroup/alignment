#' Computes the 2nd approximation of the ODF
#'
#' This function computes the 2nd approximation of the orientation
#' distribution function using the second and fourth order alignment tensors.
#' @param phi, thetha, alignment_tensor (angles in degrees)
#' @keywords orientation distribution function, ODF
#' @export
#' @examples
#' at <- matrix(c(-0.091, 0.133, 0.227,0.133, 0.114, 0.062,0.227, 0.062, -0.023),ncol=3,byrow = TRUE)
#' at4 = ...
#' odfsph4(phi,theta,atensor2,atensor4)

odfsph4 <- function(phi,theta,at2,at4){
   odf <- odfsph2(phi,theta,at2)+fsph4diff(phi,theta,at4) 
   odf
}
